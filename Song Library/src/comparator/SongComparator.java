/* Authors of Project belongs to Daniel Kim and George Ding
 * Date Completed that of 2/15/2019
 */

package comparator;

import java.util.Comparator;

import object.Song;

public class SongComparator implements Comparator<Song>{

	@Override
	public int compare(Song song1, Song song2) {
		String title1 = song1.getName().toLowerCase();
		String title2 = song2.getName().toLowerCase();

		if(title1.equals(title2)){
			String artist1 = song1.getArtist().toLowerCase();
			String artist2 = song2.getArtist().toLowerCase();

			return artist1.compareTo(artist2);
		}
		else{
			return title1.compareTo(title2);
		}

	}



}
