/* Authors of Project belongs to Daniel Kim and George Ding
 * Date Completed that of 2/15/2019
 */

package io;
import java.io.*;
import java.util.*;

import object.Song;

public class IO {

	private static File file;

	private static ArrayList<Song> list;

	private IO() {}

	public static ArrayList<Song> getList(String filePath) {

		file = new File(filePath);

		if(!file.exists()) return new ArrayList<Song>();

		Scanner scan = null;
		try {
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			//already checked. wont happen.
		}

		list = new ArrayList<Song>();

		while(scan.hasNextLine()) {

			String line = scan.nextLine();
			String[] tokens = line.split("\t");
			Song song = new Song(tokens[0], tokens[1], tokens[2], tokens[3]);
			list.add(song);
		}

		return list;
	}

	public static void writeToFile(ArrayList<Song> songList) throws IOException {

		list = songList;

		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);

		if(songList.isEmpty()){
			bw.write("");;
		}

		for(int i=0; i<list.size(); i++) {
			bw.write(list.get(i).getName() + "\t" + list.get(i).getArtist() + "\t" + list.get(i).getAlbum()
					+ "\t" + list.get(i).getDate() + "\n");
		}
		//should do popup for "successfully added"

		bw.close();
		fw.close();
	}
}
