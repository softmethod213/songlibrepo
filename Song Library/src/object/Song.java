/* Authors of Project belongs to Daniel Kim and George Ding
 * Date Completed that of 2/15/2019
 */

package object;

public class Song {
	private String name;
	private String artist;
	private String date;
	private String album;

	public Song(String name, String artist, String album, String date) {
		this.name = name;
		this.artist = artist;
		this.album = album;
		this.date = date;
		
		if(this.album.equals("")) {
			this.album = "\\null\\";
		}
		if(this.date.equals("")) {
			this.date = "\\null\\";
		}
	}

	public String getName() {
		return this.name;
	}

	public String getArtist() {
		return this.artist;
	}

	public String getDate() {
		return this.date;
	}

	public String getAlbum() {
		return this.album;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
