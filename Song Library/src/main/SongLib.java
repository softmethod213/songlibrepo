/* Authors of Project belongs to Daniel Kim and George Ding
 * Date Completed that of 2/15/2019
 */

package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.IO;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;
import object.Song;
import view.Controller;

public class SongLib extends Application{

	@FXML private static SplitPane root;
	
	private static File file = new File("./src/SongList.txt");
	private static ArrayList<Song> songList;
	
	Controller lv = new Controller();
	
	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		
		songList = IO.getList(file.getPath());
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/GUI.fxml"));
		
		try {
			
			root = (SplitPane) loader.load();
			Controller controller = loader.getController();
			controller.start(songList);
			
			arg0.setResizable(false);
			Scene scene = new Scene(root);
			arg0.setScene(scene);
			arg0.show();
		}catch (IOException e) {
			//GUI file missing
		}
	}
	
	public static void main(String[] args) throws IOException {
		
		launch(args);
		IO.writeToFile(Controller.getCurrentSongList());
	}
}
