/* Authors of Project belongs to Daniel Kim and George Ding
 * Date Completed that of 2/15/2019
 */

package view;

import javafx.event.ActionEvent;
import javafx.fxml.*;
import object.Song;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import comparator.SongComparator;
import javafx.collections.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;


public class Controller {

	private int selectedIndex = -1;

	private static ObservableList<String> titleObsList;
	private static ObservableList<String> artistObsList;

	private static ArrayList<Song> songList;

	@FXML private ListView<String> titleLV;
	@FXML private ListView<String> artistLV;

	@FXML private static Button addButton;

	@FXML private TextField titleTxt;
	@FXML private TextField artistTxt;
	@FXML private TextField albumTxt;
	@FXML private TextField dateTxt;

	@FXML private Label titleLabel;
	@FXML private Label artistLabel;
	@FXML private Label albumLabel;
	@FXML private Label dateLabel;

	public void start(ArrayList<Song> songList) {

		Controller.songList = songList;
		ArrayList<String> titleList = new ArrayList<String>();
		ArrayList<String> artistList = new ArrayList<String>();

		for(int i=0; i<songList.size(); i++) {

			titleList.add(songList.get(i).getName());
			artistList.add(songList.get(i).getArtist());
		}

		titleObsList = FXCollections.observableArrayList(titleList);
		artistObsList = FXCollections.observableArrayList(artistList);
		titleLV.setItems(titleObsList);
		artistLV.setItems(artistObsList);

		if(!(songList.isEmpty())){
			titleLV.getSelectionModel().select(0);
			artistLV.getSelectionModel().select(0);
			showSongDetails();
		}
	}

	@FXML
	public void artistLVScroll(ScrollEvent e) {

		Node n1 = artistLV.lookup(".scroll-bar:vertical");
		if(n1 instanceof ScrollBar) {
			ScrollBar s1 = (ScrollBar) n1;
			Node n2 = titleLV.lookup(".scroll-bar:vertical");
			if(n2 instanceof ScrollBar) {
				ScrollBar s2 = (ScrollBar) n2;
				s1.valueProperty().bindBidirectional(s2.valueProperty());
			}
		}
	}

	@FXML
	public void titleLVScroll(ScrollEvent e) {

		Node n1 = titleLV.lookup(".scroll-bar:vertical");
		if(n1 instanceof ScrollBar) {
			ScrollBar s1 = (ScrollBar) n1;
			Node n2 = artistLV.lookup(".scroll-bar:vertical");
			if(n2  instanceof ScrollBar) {
				ScrollBar s2 = (ScrollBar) n2;
				s1.valueProperty().bindBidirectional(s2.valueProperty());
			}
		}
	}

	@FXML
	public void editSong(ActionEvent e){

		if(titleObsList.isEmpty()){
			Alert errorAlert = new Alert(Alert.AlertType.ERROR);
			errorAlert.setContentText("Please populate the list first.");
			errorAlert.show();
			return;
		}

		selectedIndex = titleLV.getSelectionModel().getSelectedIndex();
		Song selectedSong = songList.get(selectedIndex);
		
		
		if(compare(songList, titleTxt.getText().toLowerCase(), artistTxt.getText().toLowerCase()) && !(selectedSong.getName().toLowerCase().equals(titleTxt.getText().toLowerCase()) && selectedSong.getArtist().toLowerCase().equals(artistTxt.getText().toLowerCase()))){
			Alert dup = new Alert(Alert.AlertType.ERROR);
			dup.setTitle("Song already exists.  Please change either Song name or Artist Name.");
			dup.setContentText("You have selected Song " + (selectedIndex+1) + ", but have edited it to have same Song name and Artist name as another Song.");
			dup.show();
			return;
		}else if(titleTxt.getText().isEmpty() || artistTxt.getText().isEmpty()){
			Alert emptyField = new Alert(Alert.AlertType.ERROR);
			emptyField.setTitle("Song name / Artist Name cannot be empty.");
			emptyField.setContentText("You have selected Song " + (selectedIndex+1) + ", but have left either the Song name or the Artist name empty.");
			emptyField.show();
			return;
		}else{
			Song newSong = new Song(titleTxt.getText().trim(), artistTxt.getText().trim(), albumTxt.getText().trim(), dateTxt.getText().trim());
			Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
			confirm.setContentText("Are you sure you want to edit Song Number " + (selectedIndex+1));

			Optional<ButtonType> action = confirm.showAndWait();

			if(action.get().equals(ButtonType.OK)){
				songList.remove(selectedIndex);
				songList.add(newSong);
				Collections.sort(songList, new SongComparator());
				ArrayList<String> titleList = new ArrayList<String>();
				ArrayList<String> artistList = new ArrayList<String>();

				for(int i=0; i<songList.size(); i++) {

					titleList.add(songList.get(i).getName());
					artistList.add(songList.get(i).getArtist());
				}

				titleObsList = FXCollections.observableArrayList(titleList);
				artistObsList = FXCollections.observableArrayList(artistList);
				titleLV.setItems(titleObsList);
				artistLV.setItems(artistObsList);

				selectedIndex = songList.indexOf(newSong);

				titleLV.getSelectionModel().select(selectedIndex);
				artistLV.getSelectionModel().select(selectedIndex);

				showSongDetails();
				
				titleTxt.clear(); artistTxt.clear(); albumTxt.clear(); dateTxt.clear();
				return;
			}else{
				return;
			}
		}
	}

	@FXML
	public void addSong(ActionEvent e) {
		if(compare(songList, titleTxt.getText().toLowerCase(), artistTxt.getText().toLowerCase())){
			Alert dup = new Alert(Alert.AlertType.ERROR);
			dup.setContentText("Song already exists.  Please change either Song name or Artist Name.");
			dup.show();
			titleTxt.setText("");
			artistTxt.setText("");
			albumTxt.setText("");
			dateTxt.setText("");
			return;
		}
		else if(titleTxt.getText().isEmpty() || artistTxt.getText().isEmpty()){
			Alert emptyField = new Alert(Alert.AlertType.ERROR);
			emptyField.setContentText("Song name / Artist Name cannot be empty.");
			emptyField.show();
			return;
		}
		else{
			Song newSong = new Song(titleTxt.getText().trim(), artistTxt.getText().trim(), albumTxt.getText().trim(), dateTxt.getText().trim());
			songList.add(newSong);
			Collections.sort(songList, new SongComparator());
			ArrayList<String> titleList = new ArrayList<String>();
			ArrayList<String> artistList = new ArrayList<String>();

			for(int i=0; i<songList.size(); i++) {

				titleList.add(songList.get(i).getName());
				artistList.add(songList.get(i).getArtist());
			}

			titleObsList = FXCollections.observableArrayList(titleList);
			artistObsList = FXCollections.observableArrayList(artistList);
			titleLV.setItems(titleObsList);
			artistLV.setItems(artistObsList);

			selectedIndex = songList.indexOf(newSong);

			titleLV.getSelectionModel().select(selectedIndex);
			artistLV.getSelectionModel().select(selectedIndex);
			
			titleLV.scrollTo(selectedIndex);
			artistLV.scrollTo(selectedIndex);

			showSongDetails();

			titleTxt.clear();
			artistTxt.clear();
			albumTxt.clear();
			dateTxt.clear();
			if(titleTxt.getPromptText() != "Title") {
				titleTxt.setPromptText("Title");
				artistTxt.setPromptText("Artist");
				albumTxt.setPromptText("Album");
				dateTxt.setPromptText("Date");
			}
		}

	}

	@FXML
	public void deleteSong(ActionEvent e){
		if (titleObsList.isEmpty()){
			Alert errorAlert = new Alert(Alert.AlertType.ERROR);
			errorAlert.setContentText("Please populate the list first.");
			errorAlert.show();
			return;
		}

		selectedIndex = titleLV.getSelectionModel().getSelectedIndex();

		Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
		confirm.setContentText("Are you sure you want to delete Song Number " + (selectedIndex+1) + "?");

		Optional<ButtonType> action = confirm.showAndWait();

		if(action.get().equals(ButtonType.OK)){
			songList.remove(selectedIndex);
			titleObsList.remove(selectedIndex);
			artistObsList.remove(selectedIndex);
		}
		else{
			return;
		}


		if(songList.isEmpty()){
			titleLabel.setText("");
			artistLabel.setText("");
			albumLabel.setText("");
			dateLabel.setText("");
			return;
		}
		else{
			if(selectedIndex != 0){
				titleLV.getSelectionModel().select(selectedIndex-1);
				artistLV.getSelectionModel().select(selectedIndex-1);
				titleLabel.setText(songList.get(selectedIndex-1).getName());
				artistLabel.setText(songList.get(selectedIndex-1).getArtist());
				albumLabel.setText(songList.get(selectedIndex-1).getAlbum());
				dateLabel.setText(songList.get(selectedIndex-1).getDate());
			}
			else{
				titleLV.getSelectionModel().select(selectedIndex);
				artistLV.getSelectionModel().select(selectedIndex);
				titleLabel.setText(songList.get(selectedIndex).getName());
				artistLabel.setText(songList.get(selectedIndex).getArtist());
				albumLabel.setText(songList.get(selectedIndex).getAlbum());
				dateLabel.setText(songList.get(selectedIndex).getDate());
			}
		}
		showSongDetails();
	}

	@FXML
	public void titleLVMouseClick(MouseEvent e) {

		artistLV.getSelectionModel().select(
				titleLV.getSelectionModel().getSelectedIndex());
		showSongDetails();
	}

	@FXML
	public void artistLVMouseClick(MouseEvent e) {

		titleLV.getSelectionModel().select(
				artistLV.getSelectionModel().getSelectedIndex());
		showSongDetails();
	}

	private void showSongDetails(){

		if(songList.isEmpty()){
			return;
		}
		int index = titleLV.getSelectionModel().getSelectedIndex();
		Song selectedSong = songList.get(index);

		titleLabel.setText(" Title: " + selectedSong.getName());
		artistLabel.setText(" Artist: " + selectedSong.getArtist());
		albumLabel.setText(" Album: " +
				(selectedSong.getAlbum().equals("\\null\\") ? "NA":selectedSong.getAlbum()));
		dateLabel.setText(" Date: " +
				(selectedSong.getDate().equals("\\null\\") ? "NA":selectedSong.getDate()));
	}


	public static ArrayList<Song> getCurrentSongList(){

		return songList;
	}

	private boolean compare(ArrayList<Song> list, String title, String artist){
		for(int i = 0; i < list.size(); i++){
			String songTitle = list.get(i).getName().toLowerCase();
			String artistTitle = list.get(i).getArtist().toLowerCase();
			if(songTitle.equals(title) && artistTitle.equals(artist)){
				return true;
			}
		}
		return false;
	}


}

